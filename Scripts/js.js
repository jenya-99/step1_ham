// Add listener for services-menu-link:
let links = document.getElementsByClassName('services-menu-link');
for(let i = 0; i < links.length; i++) {
  links[i].addEventListener('click', function () {
    toggleAсtiveElem(links, this);
    let items = document.getElementsByClassName('services-item');    
    for(let k = 0; k < items.length; k++) {
    items[k].classList.remove('active'); 
    }
    for(let j = 0; j < links.length; j ++) {
      if (links[j].classList.contains('active')) {
        items[j].classList.add('active');
      }
    }
  });
}
//создание объекта с массивами картинок по категориям по 12шт в каждой:
let imgObj = new CreatImgObj(12);
// Add listener for amazing-work-menu-link:
let amazingLinks = document.getElementsByClassName('amazing-work-menu-link');
for(let i = 0; i < amazingLinks.length; i++) {
  amazingLinks[i].addEventListener('click', function () {
    toggleAсtiveElem(amazingLinks, this);
    let tempItem = document.querySelector('.amazing-work-list-item').innerHTML;
    let list = document.querySelector('.amazing-work-list');
    list.innerHTML = '';
    let arrImg = imgObj[this.innerHTML];
    for(let key in arrImg) {
      let newDiv = document.createElement('DIV');
      newDiv.classList.add('amazing-work-list-item');
      newDiv.innerHTML = tempItem;
      list.appendChild(newDiv);
      let newImg = document.getElementsByClassName("amazing-work-list-item-img")[key];
      newImg.src = arrImg[key];
      let description = document.getElementsByClassName("amazing-work-list-item-description")[key];
      description.innerHTML = arrImg[key].split('/')[1];    
    }   
  });
}
// Add listener for amazing-work-btn:
let amazingBtn = document.querySelector('.amazing-work-btn');
amazingBtn.addEventListener('click', function () {
  preloadId.style.display = "block";
  setTimeout(addImg, 2000, 12, this);
});
// Создание массива объектов с отзывами:
let dataPersons = [
  { name: 'Adam Mokan',
    position: 'UX Designer',
    urlImg: 'Images/About/img0.png',
    text: 'Integer dignissim, augue tempus ul tricies luctus, quam \
    dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar \
    odio eget aliquam facilisis. Tempus ultricies luctus, quam dui \
    laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio \
    eget aliquam facilisis.'
  },
  {
    name: 'Bjarne Stroustrup',
    position: 'UI Designer',
    urlImg: 'Images/About/img1.png',
    text: 'Tempus ultricies luctus, augue tempus ul tricies luctus, quam \
    dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar \
    odio eget aliquam facilisis. Integer dignissim, quam dui \
    laoreet sem, non dictum odio nisi quis massa.'
  },
  {
    name: 'Mike Krieger',
    position: 'Business Consultant',
    urlImg: 'Images/About/img2.png',
    text: 'I am Mike Krieger, augue tempus ul tricies luctus, quam \
    dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar \
    odio eget aliquam facilisis. Integer dignissim, quam dui \
    laoreet sem, non dictum odio nisi quis massa.'
  },
  {
    name: 'James Gosling',
    position: 'Backend Developer',
    urlImg: 'Images/About/img3.png',
    text: 'I am James Gosling, ultricies luctus, augue tempus ul tricies luctus, quam \
    dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar \
    odio eget aliquam facilisis. Integer dignissim, quam dui \
    laoreet sem, non dictum odio nisi quis massa eget aliquam facilisis.'
  }, 
  {
    name: 'Augusta Byron',
    position: 'UX Designer',
    urlImg: 'Images/About/img4.png',
    text: 'I am UX Designer, quam tempus ul tricies luctus, augue  \
    dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar \
    odio eget aliquam facilisis. Integer dignissim, quam dui \
    laoreet sem, non dictum odio nisi quis massa aliquam facilisis, quam dui.'
  },
  {
    name: 'Eric Schmidt',
    position: 'Frontend Developer',
    urlImg: 'Images/About/img5.png',
    text: 'Ultricies luctus, augue tempus ul tricies luctus, eget quam \
    dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar \
    odio eget aliquam facilisis. Integer dignissim, quam nisi tempus dui \
    laoreet sem, non dictum odio quis massa aliquam facilisis ul tricies luctus.'
  },
  {
    name: 'David Nelson',
    position: 'Web Designer',
    urlImg: 'Images/About/img6.png',
    text: 'Hi, I am David Nelson, ultricies luctus, tempus ul luctus, quam \
    dui laoreet sem, non dictum odio nisi quis massa. Morbi augue pulvinar \
    odio eget aliquam facilisis. Integer dignissim, quam tricies dui \
    laoreet sem, non dictum odio nisi quis massa eget aliquam facilisis.'
  },
  {
    name: 'Hasan Ali',
    position: 'UX Designer',
    urlImg: 'Images/About/img7.png',
    text: 'UX Designer - it is ultricies luctus, tempus ul luctus, quam \
    dui laoreet sem, non dictum odio nisi quis massa. Morbi augue pulvinar \
    odio eget aliquam facilisis. Integer dignissim, quam eget tricies dui \
    laoreet sem, non facilisis dictum odio nisi quis massa aliquam.'
  },
  {
    name: 'Jack Dorsey',
    position: 'Backend Developer',
    urlImg: 'Images/About/img8.png',
    text: 'Today we have very ultricies luctus, tempus ul luctus, quam \
    dui laoreet sem, non dictum  Morbi augue dictum odio pulvinar \
    odio eget aliquam facilisis. Integer dignissim, quam eget tricies dui \
    laoreet sem, non facilisis nisi quis massa aliquam odio nisi quis massa.'
  }
];
// Генерирование элементов карусели:
dataPersons.forEach(function (elem, index) {
  //cloning item template:
  let newItem = document.querySelector('.slider-item').cloneNode(true);
  //add item content:
  newItem.children[0].children[0].src = elem.urlImg;
  newItem.dataset.img = index;
  if (index == 2) {
    personTextId.textContent = elem.text;
    personNameId.textContent = elem.name;
    personPositionId.textContent = elem.position;
    personImgId.children[0].src = elem.urlImg;
    newItem.classList.add('active');
  }
  sliderBandId.appendChild(newItem);
});
//remove template item:
document.querySelector('.slider-item').remove();
// Add listener for slider-items:
let sliderItems = document.getElementsByClassName('slider-item');
for(let i = 0; i < sliderItems.length; i++) {
  sliderItems[i].addEventListener('click', function () {
    toggleAсtiveElem(sliderItems, this);
    showPerson(this);
    if (this.dataset.img != dataPersons.length-1) {
      nextBtn.classList.add('active');
      nextBtn.style.pointerEvents = 'auto';
    }
    if (this.dataset.img == dataPersons.length-1 && this.classList.contains('active')) {
      nextBtn.classList.remove('active');
      nextBtn.style.pointerEvents = 'none';
    }
    if (this.dataset.img != 0) {
      backBtn.classList.add('active');
      backBtn.style.pointerEvents = 'auto';
    }
    if (this.dataset.img == 0 && this.classList.contains('aсtive')) {
      backBtn.classList.remove('active');
      backBtn.style.pointerEvents = 'none';
    }
  });
}
// Add listener for slider-btn-next:
let nextBtn = document.querySelector('.slider-btn-next');
let backBtn = document.querySelector('.slider-btn-back');
nextBtn.addEventListener('click', function() {
  let activeElem = document.querySelector('.slider-item.active');
  let nextAktive = document.querySelector(`li[data-img="${+activeElem.dataset.img + 1}"]`);
  nextAktive.classList.add('active');
  activeElem.classList.remove('active');
  showPerson(nextAktive);
  let marginLeft = parseInt(getComputedStyle(sliderBandId).marginLeft, 10);
  if (marginLeft > (4 - dataPersons.length) * 103) {
    sliderBandId.style.marginLeft = (marginLeft - 103) + 'px';
  }  
  let lastAktive = document.querySelector(`li[data-img="${dataPersons.length-1}"]`);
  if (lastAktive.classList.contains('active')){
  nextBtn.classList.remove('active');
  nextBtn.style.pointerEvents = 'none';
  }
  if (parseInt(sliderBandId.style.marginLeft, 10) < 0) {
    backBtn.classList.add('active');
    backBtn.style.pointerEvents = 'auto';    
  }
});
// Add listener for slider-btn-back:
backBtn.addEventListener('click', function() {
  let activeElem =  document.querySelector('.slider-item.active');
  let nextAktive = document.querySelector(`li[data-img="${+activeElem.dataset.img - 1}"]`);
  nextAktive.classList.add('active');
  activeElem.classList.remove('active');
  showPerson(nextAktive);
  let marginLeft = parseInt(getComputedStyle(sliderBandId).marginLeft, 10);
  if (marginLeft != 0) {
    sliderBandId.style.marginLeft = (marginLeft + 103) + 'px';
  }
  let firstAktive = document.querySelector(`li[data-img="0"]`);
  if (firstAktive.classList.contains('active')) {
    backBtn.classList.remove('active');
    backBtn.style.pointerEvents = 'none';
  }
  if (parseInt(sliderBandId.style.marginLeft, 10) < 0) {
    nextBtn.classList.add('active');
    nextBtn.style.pointerEvents = 'auto';
  }  
});
// Add listener for best-images-btn:
let bestImagesBtn = document.querySelector('.best-images-btn');
bestImagesBtn.addEventListener('click', function () {
  preloadId.style.display = "block";
  setTimeout(addBestImages, 2000, 3);
});